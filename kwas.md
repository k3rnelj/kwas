# Składniki

- chleb - wysuszony bochenek o wadze (przed suszeniem) >1kg (razowy na zakw/żytni/można eksperymentować)
- słód: 
  - karmelowy 10 łyżek
  - czekoladowy 2-3 łyżki (albo wcale)
  - żytni ~5 łyżek
- drożdże (suche) - takie zwykłe ze sklepu, żadne specjalistyczne, ale suche
- cukier trzcinowy - 100-150g (to niestety trzeba doregulować samemu, nie ma _dobrej_ liczby)
- woda - 10L
- (nieobowiązkowo) rodzynki (~garść)

# Algorytm

## Dzień 1

(można, nie trzeba) nieco przyrumienić wysuszony chleb w piekarniku (180st. 30 min.)

Wysuszony chleb w kawałkach  razem ze śrutowanym słodem wrzucić do naczynia, wodę (ewentualnie rozdzielić po mniejszych). Rzecz zalać wodą podgrzaną do ~90° i zostawić (przykrytą) na 24h.

## Dzień 2

Z naczynia wyłowić chleb, pozwalając mu jak najlepiej ociekać. Na koniec przelać płyn przez dość gęste sitko (aczkolwiek takie do mąki może się już przytykać), do innego naczynia, porzucając chlebowy osad z dna pierwotnego naczynia. Nie trzeba się przejmować idealnym odfiltrowaniem całego osadu, ale chodzi o to, żeby nie była to zawiesina. 

Do przefiltrowanego płynu dodać cukier, w miarę możliwości pomagając mu w rozpuszczeniu się mieszaniem/potrząsaniem/innym oddziaływaniem. W odrobinie wody o pokojowej temperaturze, ewentualnie lekko cieplejszej, rozmieszać z cukrem ilość drożdży odmierzoną poprzez rozsypanie po łyżeczce cieniutkiej warstewki drożdży o powierzchni ~(monety 50gr).  Przykryć, jeśli płyn jest w butelkach to nie zakręcać, a jedynie przyłożyć zakrętkę / kapsel.
Można wsypać rodzynki.

## Dalej

Na początku trzymać we w miarę ciepłym miejscu (szybciej dotrze do właściwego smaku). Po 24-36 godzinach płyn zaczyna smakować, kiedy ma już odpowiadający nam smak, można go schłodzić, żeby wyhamować fermentację.
Rodzynki zazwyczaj wypływają mniej więcej wtedy, kiedy zaczyna nieźle smakować
